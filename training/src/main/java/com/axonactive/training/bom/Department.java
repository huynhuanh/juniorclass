package com.axonactive.training.bom;

public class Department {
	
	private String name;
	
	private int location;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getLocation() {
		return location;
	}

	public void setLocation(int location) {
		this.location = location;
	}

	public Department(String name, int location) {
		this.name = name;
		this.location = location;
	}
	
	public Department(){
		
	}
	
}
