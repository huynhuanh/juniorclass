package com.axonactive.training.bom;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.axonactive.training.technical.validation.Email;


@XmlRootElement(name="employees")
@XmlAccessorType(XmlAccessType.FIELD)
public class Employee {

	private String employeeid;
	private String firstName;
	private String lastName;
	private String fullName;
	private int age;
	
	@NotNull
	@Email
	private String email;
	private Integer departmentid;

	public Employee(String employeeid, String firstName, String lastName, String fullName, int age, String email, Integer departmentid) {
		this.employeeid = employeeid;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.age = age;
		this.email = email;
		this.departmentid = departmentid;
	}
	
	public Employee(){}
	
	public Integer getDepartment() {
		return departmentid;
	}

	public void setDepartment(Integer department) {
		this.departmentid = department;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	

	public String getEmployeeid() {
		return employeeid;
	}

	public void setEmployeeid(String employeeid) {
		this.employeeid = employeeid;
	}

	
	
}
