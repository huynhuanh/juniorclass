package com.axonactive.training.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "employee")
@NamedQueries({ 
	@NamedQuery(name="findAll",query="SELECT e FROM EmployeeEntity e"),
	@NamedQuery(name="findByEmplyeeId", query="SELECT e FROM EmployeeEntity e where e.employeeid = :empid")
})
public class EmployeeEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	// format of one employee id have 3 parts
	// firt part: user id
	// second part: code of location (HCM or DAD) and year
	// third part: the order number (with 5 numbers)
	// for example: NQHUYHCM201300344
	@Column(name = "Employee_ID", unique = true)
	private String employeeid;

	public String getEmployeeid() {
		return employeeid;
	}

	public void setEmployeeid(String employeeid) {
		this.employeeid = employeeid;
	}

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "age")
	private int age;

	@Column(name = "email")
	private String email;

	@ManyToOne
	@JoinColumn(name = "department_id", nullable = true)
	private DepartmentEntity departments;

	public EmployeeEntity() {

	}

	public EmployeeEntity(String employeeid, String firstName, String lastName, int age, String email,
			DepartmentEntity dept) {
		super();
		this.employeeid = employeeid;
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.email = email;
		this.departments = dept;

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public DepartmentEntity getDepartments() {
		return departments;
	}

	public void setDepartments(DepartmentEntity departments) {
		this.departments = departments;
	}

}
