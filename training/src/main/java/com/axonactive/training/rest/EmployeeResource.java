package com.axonactive.training.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.axonactive.training.bom.Employee;
import com.axonactive.training.service.EmployeeService;

@Stateless
@Path("employee")
public class EmployeeResource {

	@EJB
	EmployeeService empService;
		
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public List<Employee> readAll(){
		return empService.toBoms(empService.findAll());
	}
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Response employee(@Valid Employee emp){
		empService.save(empService.toEntity(emp));
		return Response.status(Response.Status.CREATED).build();
	}
	
	@PUT
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Response updateEmployee(Employee emp){
		empService.updateEmployee(emp);
		return Response.ok().build();
	}
	
	@DELETE
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Response removeEmployee(Employee emp){
		empService.removeEmployee(emp);
		 return Response.ok().build();
	}
	
	@GET
    @Path("{EmployeeId}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Employee read(@PathParam("EmployeeId")String employeeid) {
		return empService.toBom(empService.findEmployeeById(employeeid));
    }
	
	@DELETE
    @Path("{EmployeeId}")
    public Response deleteByCode(@PathParam("EmployeeId")String employeeid) {
		 empService.removeEmployeeByEmployeeId(employeeid);
		 return Response.ok().build();
	}
}
