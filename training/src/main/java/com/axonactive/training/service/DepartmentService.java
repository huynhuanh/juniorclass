package com.axonactive.training.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;

import com.axonactive.training.bom.Department;
import com.axonactive.training.entity.DepartmentEntity;

@Stateless
public class DepartmentService extends GenericServices<DepartmentEntity, Department> {
	public DepartmentService() {
		super(DepartmentEntity.class);
	}
	
	// this method return Department object because when user input, they just need to input one department id
	// then, we can use it to save an employee with Department object 
	// Because in order to insert new employee, we need have department information for that employee
	public DepartmentEntity findDepartmentById(Integer deptId) {
		List<DepartmentEntity> getOneDepartment = em
				.createQuery("SELECT d FROM DepartmentEntity d where d.id = :deptid", DepartmentEntity.class)
				.setParameter("deptid", deptId).getResultList();

		if (getOneDepartment.isEmpty())
			throw new NoResultException("No source found");
		else
			return getOneDepartment.get(0);

	}
	
	
	@Override
	public DepartmentEntity toEntity(Department bom) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Department toBom(DepartmentEntity entity) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
