package com.axonactive.training.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

import com.axonactive.training.bom.Employee;
import com.axonactive.training.entity.EmployeeEntity;

@Stateless
public class EmployeeService extends GenericServices<EmployeeEntity, Employee> {

	@EJB
	DepartmentService deptService;

	
	
	public EmployeeService() {
		super(EmployeeEntity.class);
	}

	public void updateEmployee(Employee emp) {
		EmployeeEntity empEntity = findEmployeeById(emp.getEmployeeid());
		empEntity.setAge(emp.getAge());
		empEntity.setEmail(emp.getEmail());
		empEntity.setFirstName(emp.getFirstName());
		empEntity.setLastName(emp.getLastName());
		empEntity.setDepartments(deptService.findById(emp.getDepartment()));
		this.update(empEntity);
	}

	public void removeEmployee(Employee emp) {
		EmployeeEntity empEntity = findEmployeeById(emp.getEmployeeid());
		this.remove(empEntity.getId());
	}
	
	public void removeEmployeeByEmployeeId(String empId){
		this.removeEntity(findEmployeeById(empId));
	}
	// Because emplyeeid is unique, therefore we only find one employee 
	public EmployeeEntity findEmployeeById(String employeeid) {
		List<EmployeeEntity> getOneEmployee = new ArrayList<>();
		getOneEmployee = em
				.createNamedQuery("findByEmplyeeId", EmployeeEntity.class)
				.setParameter("empid", employeeid).getResultList();
		if(getOneEmployee.size()>0)
			return getOneEmployee.get(0);
	   return null;
	}

	public List<EmployeeEntity> findAll() {
		TypedQuery<EmployeeEntity> employeeQuery= em.createNamedQuery("findAll", EmployeeEntity.class);
		return employeeQuery.getResultList();
	}

	@Override
	public EmployeeEntity toEntity(Employee bom) {
		if (bom != null) {
			EmployeeEntity empEntity = new EmployeeEntity(bom.getEmployeeid(), bom.getFirstName(), bom.getLastName(),
					bom.getAge(), bom.getEmail(), deptService.findById(bom.getDepartment()));
			return empEntity;
		}
		return null;
	}

	@Override
	public Employee toBom(EmployeeEntity entity) {
		if (entity != null) {
			Employee emp = new Employee(entity.getEmployeeid(), entity.getFirstName(), entity.getLastName(),
					getFullName(entity), entity.getAge(), entity.getEmail(), entity.getDepartments().getId());
			return emp;
		}
		return null;
	}
	// some special business functions here

	private String getFullName(EmployeeEntity emp) {
		return emp.getFirstName() + " " + emp.getLastName();
	}

	// NQHUYHCM201300344
	public String getEmployeeLdapId(EmployeeEntity empEntity) {
		return empEntity.getEmployeeid().substring(0, empEntity.getEmployeeid().length() - 12);
	}

	public String getEmployeeLocation(EmployeeEntity empEntity) {
		return empEntity.getEmployeeid().substring(empEntity.getEmployeeid().length() - 12,
				empEntity.getEmployeeid().length() - 9);
	}

	public String getEmployeeEnteredYear(EmployeeEntity empEntity) {
		return empEntity.getEmployeeid().substring(empEntity.getEmployeeid().length() - 9,
				empEntity.getEmployeeid().length() - 5);
	}

	public String getEmployeeOrderNumber(EmployeeEntity empEntity) {
		return empEntity.getEmployeeid().substring(empEntity.getEmployeeid().length() - 5,
				empEntity.getEmployeeid().length());
	}
	// ============end some special business functions here
	// =============================

	public List<EmployeeEntity> toEntites(List<Employee> employees) {
		if (employees == null) {
			return null;
		}

		List<EmployeeEntity> employeeEntites = new ArrayList<>();
		for (Employee emp : employees) {
			if (emp != null) {
				employeeEntites.add(toEntity(emp));
			}
		}
		return employeeEntites;
	}

	public List<Employee> toBoms(List<EmployeeEntity> empEntities) {
		if (empEntities == null) {
			return null;
		}
		List<Employee> employees = new ArrayList<>();
		for (EmployeeEntity empEntity : empEntities) {
			if (empEntity != null) {
				employees.add(toBom(empEntity));
			}
		}
		return employees;
	}

}
