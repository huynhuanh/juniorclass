

//import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
//import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.support.ui.Select;

public class POLoginSuccessfulTest {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
//	System.setProperty("webdriver.firefox.bin","C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe"); 
//	System.setProperty("webdriver.gecko.driver","E:\\AAvn\\JUNIOR CLASS\\Strategic planning\\25 TESTING 1 day Week 4 VIP - Selanium to java code not finished\\003 Selanium\\geckodriver-v0.15.0-win64\\geckodriver.exe");
//	driver = new FirefoxDriver();
	
	System.setProperty("webdriver.chrome.driver", "F:\\seleniumRC\\chromedriver_win32\\chromedriver.exe");
	driver = new ChromeDriver();
	
    baseUrl = "https://myagile.org";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    driver.manage().window().maximize();
  }

  @Test
  public void testPOLoginSuccessful() throws Exception {
    driver.get(baseUrl + "/login");
    driver.findElement(By.id("username")).clear();
    driver.findElement(By.id("username")).sendKeys("huy.nguyen@axonactive.vn");
    driver.findElement(By.id("password")).clear();
    driver.findElement(By.id("password")).sendKeys("Aavn123456");
    driver.findElement(By.id("submitLogin")).click();
    Thread.sleep(2000);
    assertEquals("Product Owner View", driver.findElement(By.id("header:headerTitle")).getText());
    Thread.sleep(4000);
  //  driver.findElement(By.id("signoutlink")).click();
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }
/*
  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
  */
}
